$(function () {

    var gaugeOptions = {

        chart: {
            type: 'solidgauge',
            backgroundColor: ''
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            title: {
                y: -70
            },
            labels: {
                y: 16
            },
            minorTickInterval: null,
            tickPixelInterval: 50
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    // The speed gauge
    $('#gauge_conso').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 90,
            title: {
                text: 'Intensité'
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Intensité',
            data: [47],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                '<span style="font-size:12px;color:silver">A</span></div>'
            },
            tooltip: {
                valueSuffix: 'A'
            }
        }]

    }));

    // Bring life to the dials
    setInterval(function () {
        var chart = $('#gauge_conso').highcharts(),
            point,
            newVal,
            inc;

        if (chart) {
            point = chart.series[0].points[0];
            inc = Math.random() * (55 - 45) + 45;
            newVal = Math.round(inc);

            point.update(newVal);
        }
    }, 2000);
});

$(function () {

    var gaugeOptions = {

        chart: {
            type: 'solidgauge',
            backgroundColor: ''
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    // The cost gauge
    $('#h').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Coût'
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Coût',
            data: [15],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                '<span style="font-size:12px;color:silver">ct/h</span></div>'
            },
            tooltip: {
                valueSuffix: 'ct/h'
            }
        }]

    }));

    // Bring life to the dials
    setInterval(function () {
        var chart = $('#h').highcharts(),
            point,
            newVal,
            inc;

        if (chart) {
            point = chart.series[0].points[0];
            inc = Math.random() * (20 - 10) + 10;
            newVal = Math.round(inc);

            point.update(newVal);
        }
    }, 2000);
});

var myVar = setInterval(myTimer, 2000);

function myTimer() {
    var value = Math.round(Math.random() * (20 - 10) + 10);
    document.getElementById("gauge_cout_value").innerHTML = value
}