$(document).ready(function(){
	$(".alert-notif").hide()
	$(".car").hide()
	$('.connected-car').click(function() {
      	$('.car').slideDown()
	});

	(function($) {
    $.fn.writeText = function(content) {
        var contentArray = content.split(""),
            current = 0,
            elem = this;
        setInterval(function() {
            if(current < contentArray.length) {
                elem.text(elem.text() + contentArray[current++]);
            }
        }, 40);
    };
    
	})(jQuery);
	$("#7, #8, #9, #10").click(function(IDofObject) {
		var currentId = $(this).attr('id');
		$("#holder").writeText("Votre voiture sera prête pour " + currentId + "h (demain)");
		$(".alert-notif").show('slow');
		$(".car").hide(100)
	});

});
