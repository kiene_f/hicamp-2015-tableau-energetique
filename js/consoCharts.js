$(function () {
    var hcColor = '#697e8f';
    var hpColor = '#2b3e50';

    var consoHCData = [
        {y: 0.89, color: hcColor},
        {y: 0.69, color: hcColor},
        {y: 1.92, color: hcColor},
        {y: 0.76, color: hcColor},
        {y: 0.80, color: hcColor},
        {y: 1.02, color: hcColor},
        {y: 0.60, color: hcColor},
        {y: 0.83, color: '#c5daeb'},
        {y: 0.85, color: '#c5daeb'},
        {y: 0.74, color: '#c5daeb'}
    ];
    var consoHPData = [
        {y: 1.61, color: hpColor},
        {y: 2.55, color: hpColor},
        {y: 7.52, color: hpColor},
        {y: 1.73, color: hpColor},
        {y: 2.13, color: hpColor},
        {y: 3.44, color: hpColor},
        {y: 2.63, color: hpColor},
        {y: 1.86, color: '#96a3b0'},
        {y: 4.84, color: '#96a3b0'},
        {y: 3.41, color: '#96a3b0'}
    ];
    var weatherImages = [
        'soleil.png',
        'partnuage.png',
        'orages.png',
        'averse.png'
    ]
    $('#stackbar_conso').highcharts({
        chart: {
            type: 'column',
            marginTop: 110,
            backgroundColor:'rgba(255, 255, 255, 0.0)'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['L', 'M', 'M', 'J','V','S','D', 'L', 'M', 'M']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Coût €'
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            formatter: function () {
                return this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            },
            series: {
                pointWidth: 30,
            }
        },
        series: [{
            name: 'Heures Creuses',
            data: consoHCData,
            color: hcColor
        }, {
            name: 'Heures Pleines',
            data: consoHPData,
            color: hpColor
        }]
    }, function (chart) {

        var temperatureData = [18,19, 17, 21, 16, 22, 23, 19, 20, 17];
        var weatherData = [1,1, 3, 0, 3, 0, 0, 2, 1, 1];

        var points = chart.series[0].points;

        var imageSize = 50;
        var fontSize = 16;
        var labelHeight = fontSize + 6;
        var spacing = 5;

        $.each(points, function (pointIndex, point) {
            var consoText = chart.renderer.text(
                (consoHCData[pointIndex].y + consoHPData[pointIndex].y).toFixed(1) + "€",
                point.plotX + chart.plotLeft - 10,
                spacing + labelHeight
            ).attr({
                zIndex: 5
            }).css({
                width : '50px',
                fontSize:'16px',
                textAlign:"center"
            }).add();

            var consoTextBox = consoText.getBBox();
            consoText.attr({x: point.plotX + chart.plotLeft - consoTextBox.width/2, y: spacing + labelHeight});

            chart.renderer.image(
                './images/weather/' + weatherImages[weatherData[pointIndex]],
                point.plotX + chart.plotLeft - imageSize/2,
                spacing*2 + labelHeight + 5,
                imageSize,
                imageSize
            ).attr({
                zIndex: 5
            }).add();

            var tempText = chart.renderer.text(
                temperatureData[pointIndex] + "°",
                point.plotX + chart.plotLeft - 10,
                spacing*3 + labelHeight + imageSize + 15
            ).attr({
                zIndex: 5
            }).css({
                fontSize:'16px'
            }).add();

            var tempTextBox = tempText.getBBox();
            tempText.attr({x: point.plotX + chart.plotLeft - tempTextBox.width/2, y: spacing*3 + labelHeight + imageSize + 15});
        });

        var consoText = chart.renderer.rect(
            points[6].plotX + chart.plotLeft -30,
            chart.plotTop - 110 ,
            60,
            355,
            6
        ).attr({
            zIndex: 0
        }).css({
            color:'#949494',
            borderWidth: 4,
            borderColor: 'red',
            opacity: 0.2
        }).add();
    });

    $(function () {
        //$.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=usdeur.json&callback=?', function (data) {

            var data = [];
            var lastVal = 7;
            for ( var i = 0; i < 28000; i++ ) {
                var moy= 7;
                if(i > 7000) {
                    moy = 15;
                }
                if(i > 13000) {
                    moy = 90;
                }
                if(i > 15000) {
                    moy = 40
                }
                if(i > 20000) {
                    moy = 10;
                }
                var ran = Math.random() * 3 - 1.5;
                lastVal = lastVal + ran;
                if(lastVal < moy-9) {
                    lastVal += 0.1;
                } else if(lastVal > moy+9) {
                    lastVal -= 0.1;
                }
                if(lastVal<0) {
                    lastVal = 0.2
                }
                data[i] = [Date.UTC(2015, 10, 11, 0, 0, i*2),lastVal];

            }

            $('#timeseries_conso').highcharts({
                chart: {
                    zoomType: 'x',
                    backgroundColor : ''
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Consommation (en A)'
                    },
                    min:0
                },
                tooltip : '',
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 1,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },
                series: [{
                    type: 'area',
                    name: 'USD to EUR',
                    data: data
                }]
            });
        });
    //});
});