jQuery(document).ready(function($)
{
	$('.animate-number').each(function()
	{
		var elt = $(this);
		var targetValue = parseInt($(this).attr('data-value'));

		$({numberValue: 0}).stop().animate({numberValue: targetValue}, {
			duration: 700,
			easing: "linear",
			step: function() {
				elt.text(Math.ceil(this.numberValue));
			}
	  });
	});


});