jQuery(document).ready(function($)
{
	$('.sidebar').on('click', '#toggle-sidebar', function(e)
	{
		e.preventDefault();

		$('.sidebar').toggleClass('active');
		$('.main-content').toggleClass('sidebar-active');
	});
});