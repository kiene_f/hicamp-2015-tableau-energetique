# HiCamp - 2015 - Tableau Energ�tique #
### French
Application r�alis� dans le cadre du Hacking industry camp permettant une analyse et une gestion de la consommation de tous les appareils de la maison.

![alt text](http://francois.kiene.fr/assets/images/works/hicamp.jpg)

## Built With

* HTML5
* CSS3
* jQuery
* Bootstrap
